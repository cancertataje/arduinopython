import serial, serial.tools.list_ports


ports = list(serial.tools.list_ports.comports())

for p in ports:
    puerto = str(p).split(' ')


try:
    Conexion = serial.Serial(puerto[0], baudrate=9600)
    
    while True:
        Lectura = Conexion.readline()
        Valor = Lectura.decode('utf-8').strip()

        print(Valor)


except Exception as e:
    Conexion.close()
    print(e)