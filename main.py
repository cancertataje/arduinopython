
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import Qt, QCoreApplication
from PyQt5.QtWidgets import (QApplication, QMainWindow, QPushButton)

import serial, serial.tools.list_ports
import pyqtgraph as pg

class Ui_Main(QMainWindow):

    def __init__(self):
        super(Ui_Main, self).__init__()
        uic.loadUi("interfeaz.ui", self)

        self.BtnConectar.clicked.connect(self.Conectar)
        self.BtnDesconectar.clicked.connect(self.Desconectar)

        self.plot_widget = pg.PlotWidget()
        self.gridLayout.addWidget(self.plot_widget)

        self.plot_widget.setLabel('bottom', "Tiempo", 's')
        self.plot_widget.setLabel('left', "Distancia", 'cm')
        self.plot_widget.showGrid(x=True, y=True)

        self.x = [1]
        self.y = [0]


    
    def Conectar(self):
        try:
            ports = list(serial.tools.list_ports.comports())
            for p in ports:
                puerto = str(p).split(' ')

            self.Conexion = serial.Serial(puerto[0], baudrate=9600)

            i = 2

            while True:        
                QCoreApplication.processEvents()        
                Lectura = self.Conexion.readline()
                Valor = float(Lectura.decode('utf-8'))
                self.label_value.setText(str(Valor))

                self.y.append(Valor)
                self.x.append(i)
                i = i + 1 

                print(self.y)
                print(self.x)
                self.actualizar_grafica()


        except Exception as e:
            self.Conexion.close()
            print(e)
    
    def Desconectar(self):
        try:
            self.Conexion.close()
            self.x = [1]
            self.y = [0]
        except Exception as e:
            self.Conexion.close()
            print(e)

    
    def actualizar_grafica(self):
        self.plot_widget.clear()
        self.plot_widget.plot(self.x, self.y, pen='b', symbol='o', symbolSize=2)





if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = Ui_Main()
    window.show()
    sys.exit(app.exec_())